# Containers

This respository contains various containers and instructions for their use (if
needed). They were tested with [Podman](https://podman.io/); however, they
should work just fine with any *OCI* compliant container system. Building and
running of containers is done via a [Makefile](Makefile).

## Kubernetes

In the root directory of this repository are various *yaml* files that are
configuration files for *Kubernetes* that can be used to start the containers.
You can run them in various ways, but an example using **podman** would be:

```bash
podman play kube --build ./ibm-mq.yaml
```

That would be to start the cluster, and to stop it you'd do:

```bash
podman play kube --down ./ibm-mq.yaml
```

## Alpine

There are various *Alpine* containers with different groupings of packages
(mainly meant to speed up build time); however, one in particular is for
building an base image that packages certifications (useful for corporate
environments with their own root certs).

The container [certs-alpine](certs-alpine) builds a basic *Alpine* image
packaging the certificates in the [certs-alpine/certs](certs-alpine/certs)
folder into the image (along with a few extra utilities). To build it, add your
certificates to the `certs` directory before build.

To build the alpine images, run:

```bash
make alpine
```

## Oracle To H2

The container [oracle2h2](oracle2h2) migrates a given *Oracle* database to a
given *H2* database and transforms the data as needed with a *Python* script.
The entire database is built out during the build process and exported during
the run process. To perform this operation, create an *env* file in the
[oracle2h2](oracle2h2) folder called `config.env` that contains the following
configuration values:

```bash
INCLUDE_OBJECTS="table:SHELF,table:BOOK,sequence:SEQ_ID"
ORACLE_HOST="EXAMPLE.ORACLE.COM"
ORACLE_PASS="SECR3T"
ORACLE_SERVICE="MY.SERVICE"
ORACLE_USER="AGENT"
SCHEMA="LIBRARY"
```

Then simply run the *oracle2h2* target:

```bash
make oracle2h2
```

This will output a *tar.gz* file containing the database files.
