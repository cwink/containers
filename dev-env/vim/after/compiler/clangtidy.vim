CompilerSet makeprg=clang-tidy\ --quiet\ --format-style=file\ --extra-arg="-fshow-column"\ --extra-arg="-fshow-source-location"\ --extra-arg="-fno-caret-diagnostics"\ --extra-arg="-fno-color-diagnostics"\ --extra-arg="-fdiagnostics-format=clang"
CompilerSet errorformat=%E%f:%l:%c:\ fatal\ error:\ %m,%E%f:%l:%c:\ error:\ %m,%W%f:%l:%c:\ warning:\ %m,%I%f:%l:%c:\ note:\ %m,%-G%.%#
