let s:cpo_save = &cpo

set cpo&vim

CompilerSet makeprg=make
CompilerSet errorformat=
			\%*[^\"]\"%f\"%*\\D%l:%c:\ %m,
			\%*[^\"]\"%f\"%*\\D%l:\ %m,
			\\"%f\"%*\\D%l:%c:\ %m,
			\\"%f\"%*\\D%l:\ %m,
			\%-G%f:%l:\ %trror:\ (Each\ undeclared\ identifier\ is\ reported\ only\ once,
			\%-G%f:%l:\ %trror:\ for\ each\ function\ it\ appears\ in.),
			\%f:%l:%c:\ %trror:\ %m,
			\%f:%l:%c:\ %tarning:\ %m,
			\%f:%l:%c:\ %m,
			\%f:%l:\ %trror:\ %m,
			\%f:%l:\ %tarning:\ %m,
			\%f:%l:\ %m,
			\%f:\\(%*[^\\)]\\):\ %m,
			\\"%f\"\\,\ line\ %l%*\\D%c%*[^\ ]\ %m,
			\%-G%.%#
