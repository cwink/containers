setlocal equalprg=goimports

function! go#Lint() abort
	compiler golangci
	execute "make"
	compiler go
endfunction

nnoremap <silent><buffer> <leader>l :call go#Lint()<cr>
nnoremap <silent><buffer> <leader>m :make<cr>

compiler go
