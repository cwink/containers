setlocal equalprg=clang-format\ -style=file

function! c#Lint() abort
	compiler clangtidy
	execute "make %"
	compiler make
endfunction

nnoremap <silent><buffer> <leader>l :call c#Lint()<cr>
nnoremap <silent><buffer> <leader>m :make<cr>

compiler make
