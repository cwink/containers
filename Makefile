.PHONY: alpine dev-env dev-env-base oracle2h2 suckless vnc
.POSIX:
.SUFFIXES:

CAPP := podman

alpine:
	$(CAPP) build "$@" -t "cw-$@:latest"

dev-env: dev-env-base
	$(CAPP) build "$@" -t "cw-$@:latest"

dev-env-base: suckless
	$(CAPP) build "$@" -t "cw-$@:latest"

oracle2h2:
	$(CAPP) build "$@" \
		--no-cache \
		--secret "id=config,src=$@/config.env" \
		-t "$@:latest"
	$(CAPP) run --rm "$@:latest" >"h2.tar.gz"

suckless: vnc
	$(CAPP) build "$@" -t "cw-$@:latest"

vnc: alpine
	$(CAPP) build "$@" -t "cw-$@:latest"
