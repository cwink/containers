#!/usr/bin/python3

"""
This script parsers a given changlog XML file to an H2 compatible changlog XML file.
"""

from xml.etree.ElementTree import parse, register_namespace, XMLPullParser
from re import search
from argparse import ArgumentParser

arg_parser = ArgumentParser(
    prog="H2 Transform",
    description="Transforms a liquibase changelog XML file to an H2 changelog XML file.",
)

arg_parser.add_argument(
    "input", help="The input changelog XML file to process."
)
arg_parser.add_argument(
    "output", help="The output changelog XML file to process."
)

args = arg_parser.parse_args()

xml_parser = XMLPullParser(events=["start-ns"])

tree = parse(args.input, parser=xml_parser)

namespaces = {v[0]: v[1] for k, v in xml_parser.read_events()}

for n, u in namespaces.items():
    register_namespace(n, u)

tree = parse(args.input)

root = tree.getroot()

for e in root.findall("./changeSet/createTable/column", namespaces):
    m = search(r"VARCHAR.*\((\d+).*\)", e.get("type"))
    if m:
        e.set("type", f"VARCHAR({m.group(1)})")

for e in root.findall("./changeSet/addUniqueConstraint", namespaces):
    e.attrib.pop("forIndexName", None)

date_tables = []

for e in root.findall("./changeSet/createTable", namespaces):
    for n in e.findall("./column", namespaces):
        if n.get("type").upper().startswith("DATE"):
            date_tables.append((e.get("tableName"), n.get("name")))

for e in root.findall("./changeSet/createSequence", namespaces):
    v = e.get("maxValue")
    if v is not None and int(v) > 999999999:
        e.set("maxValue", "999999999")

for d in date_tables:
    for e in root.findall(
        f"./changeSet/insert[@tableName='{d[0]}']/column[@name='{d[1]}']",
        namespaces,
    ):
        v = e.get("valueDate")
        if v is not None:
            e.set("valueDate", v.split("T")[0])

tree.write(args.output)
