#!/bin/sh

set -e

generate()
{
	find "." -path "./.git" -prune -o \( -type d -o -type f \) -print | sort | tail -n +2 | cut -b3- | while read -r file; do
		if [ -f "${file}" ]; then
			printf "!%s\n" "${file}"
		elif [ -d "${file}" ]; then
			printf "\n"
			printf "!%s\n" "${file}"
			printf "%s/*\n" "${file}"
		fi
	done
}

printf "*\n\n"

generate | tail -n +2
