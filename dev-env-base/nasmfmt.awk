#!/usr/bin/awk -f

BEGIN {
	prev = ""
}

{
	line = tolower($0)
	gsub(/^[[:space:]]*/, "", line)
	gsub(/[[:space:]]*$/, "", line)
	if (line != "") {
		if (NR > 1 && prev != "" && line ~ /:$/) {
			printf("\n")
		}
		printf("%s%s\n", line ~ /:$/ ? "" : "\t", line)
		prev = line
	}
}
